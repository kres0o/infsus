package com.example.demo.rest;

import com.example.demo.domain.Natjecaj;
import com.example.demo.domain.Prijavnica;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/podPrijavnica")
public class PrijavnicaController {

    private static List<Prijavnica> prijavnice = new ArrayList<>();

    @PostMapping("/unos")
    public ResponseEntity<Prijavnica> unesiPrijavnicu(@RequestBody Prijavnica prijavnica) {

        return ResponseEntity.ok(prijavnica);
    }

    @GetMapping("/prijavnica/{id}")
    public ResponseEntity<Prijavnica> dohvatiPrijavnicu(@PathVariable String id) {
        Prijavnica prijavnica = new Prijavnica("1", "1", "1", "Motivacija 1", "Dokumenti 1");

        return ResponseEntity.ok(prijavnica);
    }
}
