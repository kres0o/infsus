package com.example.demo.rest;

import com.example.demo.domain.Korisnik;
import com.example.demo.domain.Prijavnica;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/podKorisnik")
public class KorisnikController {

    @PostMapping("/provjeriPodatke")
    public ResponseEntity<List<Prijavnica>> provjeriPodatke(@RequestBody Korisnik korisnik) {
        List<Prijavnica> prijavnice = new ArrayList<>();
        prijavnice.add(new Prijavnica("1", "123456789", "1", "Motivacija 1", "Dokumenti 111111111111"));
        prijavnice.add(new Prijavnica("2", "987654321", "2", "Motivacija 2", "Dokumenti 2222222222222222222222222222222222222222222222"));
        prijavnice.add(new Prijavnica("3", "111222333", "3", "Motivacija 3", "Dokumenti 3"));
        prijavnice.add(new Prijavnica("4", "444555666", "4", "Motivacija 4", "Dokumenti 42222222"));
        prijavnice.add(new Prijavnica("5", "777888999", "5", "Motivacija 5", "Dokumen"));

        return ResponseEntity.ok(prijavnice);

    }

    // Primjer podataka korisnika
    private final Korisnik primjerKorisnika = new Korisnik("1","John", "Doe", "1990-01-01", "123456789", "Hrvatska");

    @GetMapping("/korisnik/{id}") // Promjena ovdje
    public ResponseEntity<Korisnik> podKorisnika(@PathVariable String id) {
        // Vraćanje primjera podataka korisnika
        return ResponseEntity.ok(primjerKorisnika);
    }

    @PostMapping("/spremiPromjene")
    public ResponseEntity<Korisnik> spremiPromjene(@RequestBody Korisnik korisnik) {

        return ResponseEntity.ok(korisnik);
    }
}
