package com.example.demo.rest;

import com.example.demo.domain.Natjecaj;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("podNatjecaja")
public class NatjecajController {

    @GetMapping("/natjecaji")
    public ResponseEntity<List<Natjecaj>> dohvatiNatjecaje() {
        // Simulacija popisa natječaja
        List<Natjecaj> natjecaj = new ArrayList<>();
        natjecaj.add(new Natjecaj("1", "Natjecaj 1", "123", "Opis 1", "1.1.2025"));
        natjecaj.add(new Natjecaj("2", "Natjecaj 2", "456", "Opis 2", "1.1.2026"));
        natjecaj.add(new Natjecaj("3", "Natjecaj 3", "789", "Opis 3", "1.1.2027"));

        return ResponseEntity.ok(natjecaj);
    }

    @GetMapping("/natjecaj/{id}")
    public ResponseEntity<Natjecaj> dohvatiNatjecaj(@PathVariable String id) {
        // Simulacija dobivanja podataka o natječaju iz baze
        Natjecaj natjecaj = new Natjecaj("1", "Natjecaj 1", "123", "Opis 1", "1.1.2025");
        return ResponseEntity.ok(natjecaj);
    }
}
