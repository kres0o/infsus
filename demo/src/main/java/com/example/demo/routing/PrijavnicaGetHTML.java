package com.example.demo.routing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PrijavnicaGetHTML {

    @GetMapping("/prijavnica")
    public String getPage(Model model) {
        return "Prijavnica";
    }

    @GetMapping("/prijavnica/{id}")
    public String getPage(@PathVariable("id") int id, Model model) {
        model.addAttribute(id);
        return "PrijavnicaP";
    }
}
