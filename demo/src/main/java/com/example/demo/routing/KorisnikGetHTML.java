package com.example.demo.routing;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class KorisnikGetHTML {

    @GetMapping("/korisnik")
    public String getPage(Model model) {
        return "Korisnik";
    }
}
