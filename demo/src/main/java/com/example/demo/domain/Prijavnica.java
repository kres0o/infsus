package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Prijavnica {
    @Id
    private String idPrijavnice;
    private String idKorisnika;
    private String idNatjecaja;

    private String motivaciskoP;
    private String dokumenti;

    public Prijavnica(String idPrijavnice, String idKorisnika, String idNatjecaja, String motivaciskoP, String dokumenti) {
        this.idPrijavnice = idPrijavnice;
        this.idKorisnika = idKorisnika;
        this.idNatjecaja = idNatjecaja;
        this.motivaciskoP = motivaciskoP;
        this.dokumenti = dokumenti;
    }

    public String getIdPrijavnice() {
        return idPrijavnice;
    }

    public void setIdPrijavnice(String idPrijavnice) {
        this.idPrijavnice = idPrijavnice;
    }

    public String getIdKorisnika() {
        return idKorisnika;
    }

    public void setIdKorisnika(String idKorisnika) {
        this.idKorisnika = idKorisnika;
    }

    public String getIdNatjecaja() {
        return idNatjecaja;
    }

    public void setIdNatjecaja(String idNatjecaja) {
        this.idNatjecaja = idNatjecaja;
    }

    public String getMotivaciskoP() {
        return motivaciskoP;
    }

    public void setMotivaciskoP(String motivaciskoP) {
        this.motivaciskoP = motivaciskoP;
    }

    public String getDokumenti() {
        return dokumenti;
    }

    public void setDokumenti(String dokumenti) {
        this.dokumenti = dokumenti;
    }
}
