package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Korisnik {



    @Id
    private String id;
    private String ime;
    private String prezime;
    private String datR;
    private String broj;
    private String drzava;

    public Korisnik(String id, String ime, String prezime, String datR, String broj, String drzava) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.datR = datR;
        this.broj = broj;
        this.drzava = drzava;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getDatR() {
        return datR;
    }

    public void setDatR(String datR) {
        this.datR = datR;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }
}
