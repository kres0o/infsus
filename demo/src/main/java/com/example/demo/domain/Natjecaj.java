package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Natjecaj {
    @Id
    private String id;

    private String ime;
    private String broj;
    private String opis;
    private String datumZ;

    public Natjecaj(String id, String ime, String broj, String opis, String datumZ) {
        this.id = id;
        this.ime = ime;
        this.broj = broj;
        this.opis = opis;
        this.datumZ = datumZ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumZ() {
        return datumZ;
    }

    public void setDatumZ(String datumZ) {
        this.datumZ = datumZ;
    }
}
