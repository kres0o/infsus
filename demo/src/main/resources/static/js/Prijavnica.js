window.onload = function() {
    // Popuni podatke o korisniku u formi za prijavnicu
    var queryString = window.location.search;

    // Izdvoji ID natjecaja iz query stringa
    var urlParams = new URLSearchParams(queryString);
    var natjecajId = urlParams.get('idNatjecaja');
    dohvatiPodatkeOKorisniku("1");
    dohvatiNatjecaj(natjecajId);
};

function dohvatiPodatkeOKorisniku(id) {
    fetch('/podKorisnik/korisnik/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja podataka o korisniku.');
        }
        return response.json();
    })
    .then(korisnik => {
        // Popuni podatke o korisniku u formi za prijavnicu
        popuniPodatkeOKorisniku(korisnik);
    })
    .catch(error => {
        console.error(error.message);
    });
}


function popuniPodatkeOKorisniku(korisnik) {
    // Dohvati div element
    var divKorisnik = document.getElementById('divKorisnik');

    // Kreiraj tekstualni sadržaj s imenom i prezimenom korisnika
    var tekst = document.createTextNode('Ime: ' + korisnik.ime + ', Prezime: ' + korisnik.prezime);

    // Postavi tekstualni sadržaj u div element
    divKorisnik.appendChild(tekst);
}

function dohvatiNatjecaj(natjecajId) {
    fetch('/podNatjecaja/natjecaji', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja natjecanja.');
        }
        return response.json();
    })
    .then(natjecaj => {
        // Dodaj opcije natjecanja u select element
        console.log(natjecajId)
        dodajOpcijeNatjecaja(natjecaj, natjecajId);
    })
    .catch(error => {
        console.error(error.message);
    });
}

function dodajOpcijeNatjecaja(natjecaja, odabraniNatjecajId) {
    console.log(natjecaja)
    var selectNatjecaj = document.getElementById('natjecaj');

    // Očisti sve postojeće opcije
    selectNatjecaj.innerHTML = '';

    // Dodaj svako natjecanje kao opciju u select element
    natjecaja.forEach(natjecaje => {
        var option = document.createElement('option');
        option.value = natjecaje.id; // Postavite vrijednost opcije na ID natjecanja
        option.text = natjecaje.ime; // Postavite tekst opcije na naziv natjecanja
        selectNatjecaj.appendChild(option);

         if (natjecaje.id === odabraniNatjecajId) {
            option.selected = true;
         }
    });
}

document.getElementById("prijavnicaForm").addEventListener("submit", function(event) {
    event.preventDefault(); // Spriječite podnošenje forme
    submitForm();
});

function submitForm() {
    var natjecajId = document.getElementById("natjecaj").value;
    var motivacija = document.getElementById("motivacija").value;
    var dokumenti = document.getElementById("dokumenti").value;

    // Provjera jesu li motivacija i dokumenti uneseni
    if (motivacija.trim() === '') {
        alert('Molimo unesite motivacijsko pismo.');
        return;
    }
    if (dokumenti.trim() === '') {
        alert('Molimo unesite dokumente.');
        return;
    }

    var prijavica = {
        idNatjecaja: natjecajId,
        motivaciskoP: motivacija,
        dokumenti: dokumenti
    };

    fetch('/podPrijavnica/unos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(prijavica)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom slanja prijavnice.');
        }
        return response.text(); // Očekujemo tekstualni odgovor od servera
    })
    .then(data => {
        console.log(data); // Ispisujemo odgovor na konzoli
        window.location.href = '/natjecaj';
    })
    .catch(error => {
        console.error(error.message);
    });
}

