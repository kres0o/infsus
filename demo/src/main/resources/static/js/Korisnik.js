function prikaziPrijavnica() {
    // Dohvaćanje unesenih vrijednosti iz input polja
    var ime = document.getElementById('ime').value;
    var prezime = document.getElementById('prezime').value;
    var datR = document.getElementById('datR').value;
    var broj = document.getElementById('broj').value;
    var drzava = document.getElementById('drzava').value;
    // Kreiranje objekta s podacima koji će se poslati na backend
    var podaci = {
        ime: ime,
        prezime: prezime,
        datR: datR,
        broj: broj,
        drzava: drzava
    };

    // Slanje podataka na backend koristeći Fetch API
    fetch('/podKorisnik/provjeriPodatke', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(podaci)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom slanja zahtjeva.');
        }
        return response.json();
    })
    .then(data => {
        // Ovdje možete obraditi odgovor koji ste dobili od backend-a
        prijavnica(data);
    })
    .catch(error => {
        console.error(error.message);
    });
}

function prijavnica(prijavnice) {
    var prijavniceDiv = document.getElementById('prijavnice');
    prijavniceDiv.innerHTML = '';

    if (prijavnice && prijavnice.length > 0) {
        var lista = document.createElement('ul');
        prijavnice.forEach(function(prijavnica) {
            var listItem = document.createElement('li');
            var prijavnicaInfo = document.createElement('p');
            prijavnicaInfo.textContent = 'Natjecaj: ' + prijavnica.idNatjecaja + ',    Dokumenti: ' + prijavnica.dokumenti;

            var infoButton = document.createElement('button');
            infoButton.textContent = 'Detaljji';
            infoButton.addEventListener('click', function() {
                window.location.href = '/prijavnica/' + prijavnica.idPrijavnice;
            });

            listItem.appendChild(prijavnicaInfo);
            listItem.appendChild(infoButton);
            lista.appendChild(listItem);
        });
        prijavniceDiv.appendChild(lista);
    } else {
        prijavniceDiv.textContent = 'Nema dostupnih prijavnica.';
    }
}



window.onload = function() {
    // Dohvati podatke o korisniku i popuni ih u formu
    dohvatiPodatkeOKorisniku(1);
};

function dohvatiPodatkeOKorisniku(id) {
    fetch('/podKorisnik/korisnik/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja podataka o korisniku.');
        }
        return response.json();
    })
    .then(korisnik => {
        // Popuni podatke o korisniku u formu
        popuniPodatke(korisnik);
    })
    .catch(error => {
        console.error(error.message);
    });
}

function popuniPodatke(korisnik) {
    // Popunite formu s podacima o korisniku
    document.getElementById('ime').value = korisnik.ime;
    document.getElementById('prezime').value = korisnik.prezime;
    document.getElementById('datR').value = korisnik.datR;
    document.getElementById('broj').value = korisnik.broj;
    document.getElementById('drzava').value = korisnik.drzava;
}

function spremiPromjene() {
    // Dohvaćanje unesenih vrijednosti iz input polja
    var ime = document.getElementById('ime').value;
    var prezime = document.getElementById('prezime').value;
    var datR = document.getElementById('datR').value;
    var broj = document.getElementById('broj').value;
    var drzava = document.getElementById('drzava').value;

    // Provjera ispravnosti unesenih podataka
    if (ime.trim() === '') {
        alert('Molimo unesite ime.');
        return;
    }
    if (prezime.trim() === '') {
        alert('Molimo unesite prezime.');
        return;
    }
    if (!isValidDate(datR)) {
        alert('Molimo unesite ispravan datum rođenja u obliku YYYY-MM-DD.');
        return;
    }
    if (!isValidPhoneNumber(broj)) {
        alert('Molimo unesite ispravan broj telefona (9 ili 10 brojeva).');
        return;
    }
    if (drzava.trim() === '') {
        alert('Molimo unesite državu.');
        return;
    }

    // Ako su podaci ispravni, nastavljamo s procesom spremanja promjena
    var podaci = {
        ime: ime,
        prezime: prezime,
        datR: datR,
        broj: broj,
        drzava: drzava
    };

    fetch('/podKorisnik/spremiPromjene', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(podaci)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom slanja zahtjeva za spremanje promjena.');
        }
        alert('Promjene su uspješno spremljene.');
    })
    .catch(error => {
        console.error(error.message);
        alert('Došlo je do greške prilikom spremanja promjena.');
    });
}

// Funkcija za provjeru ispravnosti datuma
function isValidDate(dateString) {
    // Regex za provjeru formata YYYY-MM-DD
    var regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(dateString);
}

// Funkcija za provjeru ispravnosti broja telefona
function isValidPhoneNumber(phoneNumber) {
    // Provjeravamo je li broj sastavljen od 9 ili 10 brojeva
    return /^\d{9,10}$/.test(phoneNumber);
}

