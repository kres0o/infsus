var natjecajInfo = {}

window.onload = function() {
    dohvatiNatjecaje();
};

function dohvatiNatjecaje() {
    fetch('/podNatjecaja/natjecaji', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja natjecaja.');
        }
        return response.json();
    })
    .then(natjecaji => {
        popuniListuNatjecaja(natjecaji);
    })
    .catch(error => {
        console.error(error.message);
    });
}

function popuniListuNatjecaja(natjecaji) {
    var listaNatjecaja = document.getElementById('listaNatjecaja');

    // Očisti listu prije dodavanja novih natjecaja
    listaNatjecaja.innerHTML = '';

    // Iteriraj kroz natjecaje i dodaj ih u listu
    natjecaji.forEach(natjecaj => {
        var natjecajItem = document.createElement('li');
        natjecajItem.textContent = natjecaj.ime; // Postavljamo naziv natjecaja kao tekst stavke u listi
        natjecajItem.addEventListener('click', function() {
            prikaziDetaljeNatjecaja(natjecaj);
        });
        listaNatjecaja.appendChild(natjecajItem);
    });
}

function prikaziDetaljeNatjecaja(natjecaj) {
    // Kreiramo ili ažuriramo element za prikaz detalja natjecaja
    natjecajInfo = natjecaj
    var detaljiNatjecaja = document.getElementById('detaljiNatjecaja');
    if (!detaljiNatjecaja) {
        detaljiNatjecaja = document.createElement('div');
        detaljiNatjecaja.id = 'detaljiNatjecaja';
        document.body.appendChild(detaljiNatjecaja);
    }

    // Popunjavamo detalje natjecaja
    detaljiNatjecaja.innerHTML = `
        <h3>${natjecaj.ime}</h3>
        <p><strong>Opis:</strong> ${natjecaj.opis}</p>
        <p><strong>Kontakt broj:</strong> ${natjecaj.broj}</p>
        <p><strong>Datum završetka:</strong> ${natjecaj.datumZ}</p>
        <!-- Dodajte ostale detalje natjecaja ovdje -->
        <button onclick="otvoriPrijavnicu()">Izradi prijavnicu</button>
    `;
}

function otvoriPrijavnicu() {
    // Preusmjeri korisnika na stranicu za izradu prijavnice s odabranim natjecajem
    var url = '/prijavnica?idNatjecaja=' + natjecajInfo.id;

    window.location.href = url;
}
