var prijavicaInfo = {};
var natjecajInfo = {};
var korisnikInfo = {};

window.onload = function() {
    // Dohvati dio URL-a koji sadrži broj (npr. "/1")
    var path = window.location.pathname;
    // Izdvoji broj iz dijela URL-a
    var prijavnicaId = path.substring(path.lastIndexOf('/') + 1);
    console.log(prijavnicaId)
    // Pozovi funkciju za dohvat podataka o prijavnici s dobivenim ID-om
    dohvatiPodatkeOPrijavnici(prijavnicaId);
};

function dohvatiPodatkeOKorisniku(id) {
    fetch('/podKorisnik/korisnik/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja podataka o korisniku.');
        }
        return response.json();
    })
    .then(korisnik => {
        korisnikInfo = korisnik;
        popuniPodatkeOKorisniku(korisnik);
    })
    .catch(error => {
        console.error(error.message);
    });
}


function dohvatiPodatkeONatjecaju(id) {
    fetch('/podNatjecaja/natjecaj/' + id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja natjecaja.');
        }
        return response.json();
    })
    .then(natjecaj => {
        natjecajInfo = natjecaj;
        popuniPodatkeONatjecaju(natjecaj);
    })
    .catch(error => {
        console.error(error.message);
    });
}

function popuniPodatkeOKorisniku(korisnik) {
    var divKorisnik = document.getElementById('divKorisnik');
    var korisnikInfo = document.createElement('p');
    korisnikInfo.textContent = 'Ime: ' + korisnik.ime + ', Prezime: ' + korisnik.prezime;
    divKorisnik.appendChild(korisnikInfo);
}

function popuniPodatkeONatjecaju(natjecaj) {
    var divNatjecaj = document.getElementById('divNatjecaj');
    var natjecajInfo = document.createElement('p');
    natjecajInfo.textContent = 'Naziv natjecaja: ' + natjecaj.ime;
    divNatjecaj.appendChild(natjecajInfo);
}

function popuniPodatkeOPrijavnici(prijavnica) {
    document.getElementById('motivacija').value = prijavnica.motivaciskoP;
    document.getElementById('dokumenti').value = prijavnica.dokumenti;
}

function dohvatiPodatkeOPrijavnici(id) {
    fetch('/podPrijavnica/prijavnica/' + id, { // Dodajemo ID u rutu
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom dohvaćanja podataka o prijavnici.');
        }
        return response.json();
    })
    .then(prijavnica => {
        // Ovdje možete koristiti dobivene podatke o prijavnici
        // Na primjer, možete ih prikazati ili ih dalje obraditi
        console.log(prijavnica);
        prijavicaInfo = prijavnica;
        popuniPodatkeOPrijavnici(prijavnica);
        dohvatiPodatkeONatjecaju(prijavnica.idNatjecaja);
        dohvatiPodatkeOKorisniku(prijavnica.idKorisnika);
    })
    .catch(error => {
        console.error(error.message);
    });
}

document.getElementById('prijavnicaForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Spriječite podnošenje obrasca

    var motivacija = document.getElementById('motivacija').value;
    var dokumenti = document.getElementById('dokumenti').value;

    // Provjerite jesu li motivacija i dokumenti uneseni
    if (motivacija.trim() === '') {
        alert('Molimo unesite motivacijsko pismo.');
        return;
    }
    if (dokumenti.trim() === '') {
        alert('Molimo unesite dokumente.');
        return;
    }

    // Kreirajte objekt s podacima o prijavnici
    var data = {
        motivacija: motivacija,
        dokumenti: dokumenti
    };

    // Pošaljite POST zahtjev na backend
    fetch('/podPrijavnica/unos', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Došlo je do greške prilikom pohrane prijavnice.');
        }
        return response.json();
    })
    .then(prijavnica => {
        // Ovdje možete obraditi odgovor od servera nakon uspješne pohrane prijavnice
        console.log('Prijavnica uspješno pohranjena:', prijavnica);
        window.location.href = '/korisnik';
    })
    .catch(error => {
        console.error(error.message);
    });
});


document.getElementById("obrisiPrijavnicu").addEventListener("click", function(event) {
    event.preventDefault(); // Spriječite podnošenje forme
    obrisiPrijavnicu(prijavicaInfo);
});

function obrisiPrijavnicu(prijavnica) {
    console.log(prijavnica)
    console.log(prijavnica.idPrijavnice)
    window.location.href = '/korisnik';
}



